#!/usr/bin/env bash

set -e

wget -q https://releases.hashicorp.com/boundary/${BOUNDARY_VERSION}/boundary_${BOUNDARY_VERSION}_linux_amd64.zip
wget -q https://releases.hashicorp.com/boundary/${BOUNDARY_VERSION}/boundary_${BOUNDARY_VERSION}_SHA256SUMS
wget -q https://releases.hashicorp.com/boundary/${BOUNDARY_VERSION}/boundary_${BOUNDARY_VERSION}_SHA256SUMS.sig
wget -q https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy
chmod +x cloud_sql_proxy
sudo mv ./cloud_sql_proxy /usr/bin

gpg --import /tmp/hashicorp.asc
gpg --verify boundary_${BOUNDARY_VERSION}_SHA256SUMS.sig
sha256sum --check --ignore-missing boundary_${BOUNDARY_VERSION}_SHA256SUMS

sudo unzip -d /usr/bin boundary_${BOUNDARY_VERSION}_linux_amd64.zip
rm -rf *.zip

sudo mkdir --parents /etc/boundary

# sudo cp -rvn /tmp/boundary/conf/* /etc/boundary
sudo cp -rvn /tmp/boundary/systemd/* /etc/systemd/system

sudo systemctl daemon-reload
sudo systemctl enable boundary.path
sudo systemctl enable boundary.service

sync
