#!/usr/bin/env bash

set -e

sleep 60

sudo apt update
sudo apt upgrade -y
sudo apt install -y unzip apt-transport-https ca-certificates curl gnupg-agent software-properties-common jq

sync
