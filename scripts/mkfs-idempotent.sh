#!/usr/bin/bash

# Creates a filesystem on the given block device if one does not exist

# This script provides an idempotent way of formatting a block device
# with a desired filesystem. The assumptions are that the filesystem
# is a type that supports labeling and that the label will be properly
# discovered by findfs. Once the block device has been formatted, it
# can be used in, for example, systemd.mount(5) units.

if [[ -z "${FILESYSTEM_LABEL}" ]]; then
  echo "Error: Required environment variable FILESYSTEM_LABEL not set."
  exit 1
fi

if [[ -z "${BLOCK_DEVICE}" ]]; then
  echo "Error: Required environment variable BLOCK_DEVICE not set."
  exit 1
fi

if [[ -z "${FILESYSTEM_TYPE}" ]]; then
  echo "Error: Required environment variable FILESYSTEM_TYPE not set."
  exit 1
fi

findfs LABEL=${FILESYSTEM_LABEL} 2>/dev/null

STATUSCODE=$?

if test $STATUSCODE -eq 1; then
  case ${FILESYSTEM_TYPE} in
    bfs)
      echo "Error: SCO bfs filesystems are not supported because the world has moved on."
      exit 1
      ;;
    btrfs)
      mkfs.btrfs -L "${FILESYSTEM_LABEL}" "${BLOCK_DEVICE}"
      ;;
    cramfs)
      mkfs.cramfs -n "${FILESYSTEM_LABEL}" "${BLOCK_DEVICE}"
      ;;
    ext2)
      mkfs.ext2 -L "${FILESYSTEM_LABEL}" "${BLOCK_DEVICE}"
      ;;
    ext3)
      mkfs.ext3 -L "${FILESYSTEM_LABEL}" "${BLOCK_DEVICE}"
      ;;
    ext4)
      sudo mkfs.ext4 -L "${FILESYSTEM_LABEL}" "${BLOCK_DEVICE}"
      ;;
    fat)
      mkfs.fat -n "${FILESYSTEM_LABEL}" "${BLOCK_DEVICE}"
      ;;
    vfat)
      mkfs.vfat -n "${FILESYSTEM_LABEL}" "${BLOCK_DEVICE}"
      ;;
    minix)
      echo "Error: Minix filesystems are not supported because they cannot be labeled."
      exit 1
      ;;
    msdos)
      mkfs.msdos -n "${FILESYSTEM_LABEL}" "${BLOCK_DEVICE}"
      ;;
    ntfs)
      mkfs.ntfs -L "${FILESYSTEM_LABEL}" "${BLOCK_DEVICE}"
      ;;
    xfs)
      mkfs.xfs -L "${FILESYSTEM_LABEL}" "${BLOCK_DEVICE}"
      ;;
  esac
fi
