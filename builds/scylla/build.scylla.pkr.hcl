build {
  name = "scylla"

  sources = [
    "source.googlecompute.scylla",
  ]

  provisioner "shell" {
    inline = [
      "sleep 30",
      "sudo apt update",
      "sudo apt install -y ansible",
    ]
  }

  provisioner "ansible-local" {
    playbook_dir  = "ansible"
    playbook_file = "ansible/scylla.yml"

    extra_arguments = [
      "--extra-vars", "consul_version=${var.consul_version}",
    ]
  }
}
