variable "project" {
  type = string
}

variable "zone" {
  type = string
}

variable "source_image_family" {
  type = string
}

variable "ssh_username" {
  type = string
}

variable "consul_version" {
  type = string
}
