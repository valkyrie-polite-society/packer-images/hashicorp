build {
  name = "scheduler"

  sources = [
    "source.googlecompute.scheduler",
  ]

  provisioner "shell" {
    inline = [
      "sleep 30",
      "sudo apt update",
      "sudo apt install -y ansible",
    ]
  }

  provisioner "ansible-local" {
    playbook_dir  = "ansible"
    playbook_file = "ansible/scheduler.yml"

    extra_arguments = [
      "--extra-vars", "consul_version=${var.consul_version}",
      "--extra-vars", "vault_version=${var.vault_version}",
      "--extra-vars", "nomad_version=${var.nomad_version}",
      "--extra-vars", "consul_agent_mode=client",
      "--extra-vars", "nomad_agent_mode=server",
    ]
  }
}
