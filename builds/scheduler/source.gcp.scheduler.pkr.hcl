source "googlecompute" "scheduler" {
  project_id           = var.project
  zone                 = var.zone
  source_image_family  = var.source_image_family
  ssh_username         = var.ssh_username
  image_name           = "packer-${uuidv4()}"
  image_family         = "scheduler"
  image_description    = "Consul for service discovery and Nomad for scheduling workloads"
}
