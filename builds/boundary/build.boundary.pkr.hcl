build {
  name = "boundary"

  sources = [
    "source.googlecompute.boundary",
  ]

  provisioner "file" {
    source      = "./gpg-keys/hashicorp.asc"
    destination = "/tmp/hashicorp.asc"
  }

  provisioner "file" {
    source      = "boundary"
    destination = "/tmp"
  }

  provisioner "shell" {
    environment_vars = [
      "BOUNDARY_VERSION=${var.boundary_version}",
    ]

    scripts = [
      "./scripts/install-basic-software.sh",
      "./scripts/install-boundary.sh",
    ]
  }
}
