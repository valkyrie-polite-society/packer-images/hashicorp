build {
  name = "compute"

  sources = [
    "source.googlecompute.compute",
  ]

  provisioner "shell" {
    inline = [
      "sleep 30",
      "sudo apt update",
      "sudo apt install -y ansible",
    ]
  }

  provisioner "ansible-local" {
    playbook_dir  = "ansible"
    playbook_file = "ansible/compute.yml"

    extra_arguments = [
      "--extra-vars", "consul_version=${var.consul_version}",
      "--extra-vars", "vault_version=${var.vault_version}",
      "--extra-vars", "nomad_version=${var.nomad_version}",
      "--extra-vars", "cni_plugin_version=${var.cni_plugin_version}",
      "--extra-vars", "consul_agent_mode=client",
      "--extra-vars", "nomad_agent_mode=client",
    ]
  }
}
