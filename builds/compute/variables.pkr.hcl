variable "project" {
  type = string
}

variable "zone" {
  type = string
}

variable "source_image_family" {
  type = string
}

variable "ssh_username" {
  type = string
}

variable "consul_version" {
  type = string
}

variable "vault_version" {
  type = string
}

variable "nomad_version" {
  type = string
}

variable "boundary_version" {
  type = string
}

variable "cni_plugin_version" {
  type = string
}
