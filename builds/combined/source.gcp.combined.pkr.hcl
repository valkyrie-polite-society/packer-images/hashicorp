source "googlecompute" "combined" {
  project_id           = var.project
  zone                 = var.zone
  source_image_family  = var.source_image_family
  ssh_username         = var.ssh_username
  image_name           = "packer-${uuidv4()}"
  image_family         = "combined"
  image_description    = "Consul for configuration, Vault for secrets, and Nomad and Docker for running workloads"
}
