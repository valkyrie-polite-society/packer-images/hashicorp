build {
  name = "combined"

  sources = [
    "source.googlecompute.combined",
  ]

  provisioner "file" {
    source      = "./gpg-keys/hashicorp.asc"
    destination = "/tmp/hashicorp.asc"
  }

  provisioner "file" {
    source      = "consul"
    destination = "/tmp"
  }

  provisioner "file" {
    source      = "vault"
    destination = "/tmp"
  }

  provisioner "file" {
    source      = "nomad"
    destination = "/tmp"
  }

  provisioner "shell" {
    environment_vars = [
      "CONSUL_VERSION=${var.consul_version}",
      "VAULT_VERSION=${var.vault_version}",
      "NOMAD_VERSION=${var.nomad_version}",
      "CONSUL_AGENT_MODE=server",
    ]

    scripts = [
      "./scripts/install-basic-software.sh",
      "./scripts/install-consul.sh",
      "./scripts/install-vault.sh",
      "./scripts/install-nomad.sh",
      "./scripts/install-docker.sh",
    ]
  }
}
