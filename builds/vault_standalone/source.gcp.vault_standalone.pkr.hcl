source "googlecompute" "vault_standalone" {
  project_id           = var.project
  zone                 = var.zone
  source_image_family  = var.source_image_family
  ssh_username         = var.ssh_username
  image_name           = "packer-${uuidv4()}"
  image_family         = "vault-standalone"
  image_description    = "Vault using integrated storage"
}
