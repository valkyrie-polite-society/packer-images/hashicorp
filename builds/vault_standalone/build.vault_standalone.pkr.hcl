build {
  name = "vault_standalone"

  sources = [
    "source.googlecompute.vault_standalone",
  ]

  provisioner "shell" {
    inline = [
      "sleep 30",
      "sudo apt update",
      "sudo apt install -y ansible",
    ]
  }

  provisioner "ansible-local" {
    playbook_dir  = "ansible"
    playbook_file = "ansible/vault-standalone.yml"

    extra_arguments = [
      "--extra-vars", "vault_version=${var.vault_version}",
      "--extra-vars", "consul_version=${var.consul_version}",
      "--extra-vars", "consul_agent_mode=client",
    ]
  }
}
