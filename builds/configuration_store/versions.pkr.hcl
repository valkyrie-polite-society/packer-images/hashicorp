packer {
  required_plugins {
    googlecompute = {
      version = ">= 1.0.15"
      source  = "github.com/hashicorp/googlecompute"
    }
  }
}
