build {
  name = "configuration-store"

  sources = [
    "source.googlecompute.configuration_store",
  ]

  provisioner "shell" {
    inline = [
      "sleep 30",
      "sudo apt update",
      "sudo apt install -y ansible",
    ]
  }

  provisioner "ansible-local" {
    playbook_dir  = "ansible"
    playbook_file = "ansible/configuration-store.yml"

    extra_arguments = [
      "--extra-vars", "consul_version=${var.consul_version}",
      "--extra-vars", "consul_agent_mode=server",
    ]
  }
}
