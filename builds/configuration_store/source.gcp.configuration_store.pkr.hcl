source "googlecompute" "configuration_store" {
  project_id           = var.project
  zone                 = var.zone
  source_image_family  = var.source_image_family
  ssh_username         = var.ssh_username
  image_name           = "packer-${uuidv4()}"
  image_family         = "configuration-store"
  image_description    = "Consul for configuration storage, and service discovery and Vault for secrets storage"
}
