check:*.pkr.hcl
	packer validate .

all: check
	packer build .
