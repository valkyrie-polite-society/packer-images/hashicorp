tls {
  http = true
  rpc  = true

  ca_file   = "/etc/nomad/ca.pem"
  cert_file = "/etc/nomad/tls.crt"
  key_file  = "/etc/nomad/tls.key"
}
