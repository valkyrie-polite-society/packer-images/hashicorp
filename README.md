# HashiCorp

This project defines Packer builds for four images: `configuration-store`, `vault-standalone`, `scheduler`, and `compute`. Each of these provide the basis for running a control plane and a pool of container worker nodes running various compute workloads. They are all configured with systemd path-based unit activation in mind. The architect is free to configure the cluster as she desires, but at least the required files mentioned in the sections below must be provided for the Consul, Vault, and Nomad services to start.

## Image `configuration-store`

This image runs a Consul cluster which can be used for configuration storage, service mesh, backing storage for Vault, or whatever the architect desires.

### Required Files

| Path                         | Description  |
|------------------------------|---|
| `/etc/consul/ca.pem`  | The root certificate authority that Vault will trust. |
| `/etc/consul/tls.key` | A TLS private key in PEM format. |
| `/etc/consul/tls.crt`   | A TLS certificate in PEM. Intermediate and root certificate authorities may be appended. Each certificate is newline-separated.  |
| `/etc/consul/gossip.json` | A gossip encryption key. Easily generate with `consul keygen` |
| `/etc/consul/retry-join.json` | The manner in which server peers will be found |

## Image `vault-standalone`

This image runs a Vault cluster which can be used for static secrets storage or dynamic secrets across cloud providers and data stores.

### Required Files

| Path                         | Description  |
|------------------------------|---|
| `/etc/vault/ca.pem` | The root certificate authority that Vault will trust. |
| `/etc/vault/tls.key` | A TLS private key in PEM format. |
| `/etc/vault/tls.crt`   | A TLS certificate in PEM. Intermediate and root certificate authorities may be appended. Each certificate is newline-separated.  |
| `/etc/vault/storage.hcl`     | The backing [storage configuration](https://www.vaultproject.io/docs/configuration/storage) for the Vault cluster. |

## Images `scheduler` and `compute`

The `scheduler` image runs a Nomad cluster which allows the architect to schedule containerized jobs and services to run on compute resources. The `compute` image runs a Nomad client that manages Docker for actually running the workloads.

### Required Files

| Path                         | Description  |
|------------------------------|---|
| `/etc/consul/ca.pem`  | The root certificate authority that Vault will trust. |
| `/etc/consul/tls.key` | A TLS private key in PEM format. |
| `/etc/consul/tls.crt`   | A TLS certificate in PEM. Intermediate and root certificate authorities may be appended. Each certificate is newline-separated.  |
| `/etc/consul/gossip.json` | A gossip encryption key. Easily generate with `consul keygen` |
| `/etc/consul/retry-join.json` | The manner in which server peers will be found |
| `/etc/nomad/tls.key` | A TLS private key in PEM format. |
| `/etc/nomad/tls.crt` | A TLS certificate in PEM. Intermediate and root certificate authorities may be appended. Each certificate is newline-separated.  |
| `/etc/nomad/ca.pem` | The root certificate authority that Vault will trust. |

## Image `combined`

The `combined` image runs Consul, Vault, Nomad, and Docker to provide a container compute platform on the fewest virtual machines possible.

### Required Files

| Path                         | Description  |
|------------------------------|---|
| `/etc/consul/ca.pem`  | The root certificate authority that Vault will trust. |
| `/etc/consul/tls.key` | A TLS private key in PEM format. |
| `/etc/consul/tls.crt`   | A TLS certificate in PEM. Intermediate and root certificate authorities may be appended. Each certificate is newline-separated.  |
| `/etc/consul/gossip.json` | A gossip encryption key. Easily generate with `consul keygen` |
| `/etc/consul/retry-join.json` | The manner in which server peers will be found |
| `/etc/vault/ca.pem` | The root certificate authority that Vault will trust. |
| `/etc/vault/tls.key` | A TLS private key in PEM format. |
| `/etc/vault/tls.crt`   | A TLS certificate in PEM. Intermediate and root certificate authorities may be appended. Each certificate is newline-separated.  |
| `/etc/vault/storage.hcl`     | The backing [storage configuration](https://www.vaultproject.io/docs/configuration/storage) for the Vault cluster. |
| `/etc/nomad/tls.key` | A TLS private key in PEM format. |
| `/etc/nomad/tls.crt` | A TLS certificate in PEM. Intermediate and root certificate authorities may be appended. Each certificate is newline-separated.  |
| `/etc/nomad/ca.pem` | The root certificate authority that Vault will trust. |
